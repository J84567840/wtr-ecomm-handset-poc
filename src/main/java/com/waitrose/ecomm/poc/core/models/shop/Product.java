package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@ToString
@EqualsAndHashCode //NOSONAR
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class Product implements Serializable {

    private String __typename;
    private String id;
    private String lineNumber;
    private String productType;
    // NOTE - [AB] EAN exists in TrolleyItem; no need to repeat it here. It is used internally to pass EAN from
    // the Product Search results to the TrolleyItem in ShopTrolleyController
    private String ean;
    private String brand;
    private String name;
    private String shortName;
    private ProductImage productImageUrls;
    private String sizeDescription;
}
