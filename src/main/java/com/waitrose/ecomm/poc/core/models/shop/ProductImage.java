package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@ToString
@EqualsAndHashCode //NOSONAR
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class ProductImage implements Serializable {

    private String __typename;
    private String extraLarge;
    private String large;
    private String medium;
    private String small;
}
