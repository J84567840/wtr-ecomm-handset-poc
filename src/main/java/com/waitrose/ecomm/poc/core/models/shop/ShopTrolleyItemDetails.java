package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Builder
public class ShopTrolleyItemDetails {

    private String __typename;

    private String lineNumber;

    private String ean;

    private Quantity quantity;

    private QuantityPrice retailPrice;

    private Price saving;

    private Price totalPrice;

    private int itemId;

    private ShopTrolleyPromotion[] promotions;

    private ShopTrolleyConflict[] conflicts;

    private Product productDetails;

    private Date lastScanTimestamp;

}
