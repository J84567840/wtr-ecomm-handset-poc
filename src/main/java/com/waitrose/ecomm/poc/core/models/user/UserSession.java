package com.waitrose.ecomm.poc.core.models.user;

public record UserSession(
        String __typename,
        String accessToken,
        String refreshToken,
        String customerId,
        String customerOrderId,
        String customerOrderState,
        String defaultBranchId,
        Integer expiresIn) {
}
