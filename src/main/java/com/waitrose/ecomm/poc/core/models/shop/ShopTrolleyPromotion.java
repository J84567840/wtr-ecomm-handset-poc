package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class ShopTrolleyPromotion implements Serializable {

    private String __typename;

    private String promotionId;

    private String promotionDescription;

    private String promotionStatus;
}
