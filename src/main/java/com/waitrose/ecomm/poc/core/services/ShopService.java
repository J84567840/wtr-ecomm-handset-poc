package com.waitrose.ecomm.poc.core.services;

import com.waitrose.ecomm.poc.core.models.shop.BranchId;
import com.waitrose.ecomm.poc.core.models.user.MyWaitroseCardNo;
import com.waitrose.ecomm.poc.core.models.event.NotificationType;
import com.waitrose.ecomm.poc.core.models.event.ShopEventWrapper;
import com.waitrose.ecomm.poc.core.models.shop.ShopDetails;
import com.waitrose.ecomm.poc.core.models.user.UserSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class ShopService {

    private final NotificationService notificationService;
    private final CustomerService customerService;
    private final ScanShopService scanShopService;

    public void createShop(MyWaitroseCardNo myWaitroseCardNo, BranchId branchId) {
        UserSession userSession = customerService.createAuthenticationSession(myWaitroseCardNo);

        ShopDetails shopDetailsResponse = scanShopService.createShop(myWaitroseCardNo, branchId, userSession);

        ShopEventWrapper shopEventWrapper = new ShopEventWrapper(shopDetailsResponse);
        notificationService.send(shopEventWrapper, NotificationType.SHOP);
    }
}