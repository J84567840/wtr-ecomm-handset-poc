package com.waitrose.ecomm.poc.core.models.shop;

public record BranchId(String value) {
}
