package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor //NOSONAR
@Getter
@Builder //NOSONAR
public class ShopTrolleyConflict implements Serializable {

    private String __typename;

    private Integer priority;

    private String code;

    private String message;

    private Map<String, String> messageVariations;

    private List<Map<String, String>> dataValues;

    private String[] resolutionActions;

    private String[] prohibitedActions;

    private String type;

    private Boolean addToTrolley;
}
