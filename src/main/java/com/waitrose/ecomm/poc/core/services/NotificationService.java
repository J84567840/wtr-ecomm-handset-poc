package com.waitrose.ecomm.poc.core.services;

import com.waitrose.ecomm.poc.core.models.event.EventSubscription;
import com.waitrose.ecomm.poc.core.models.event.NotificationType;

public interface NotificationService {

    EventSubscription subscribeToEvents();

    <T> void send(T notification, NotificationType notificationType);
}
