package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Builder
public class ShopTrolleyDetails {

    private String __typename;

    private String trolleyId;

    private String branchId;

    private ShopTrolleyTotals totals;

    private ShopTrolleyItemDetails[] items;
    
    private Integer noOfReturnedItems;
}
