package com.waitrose.ecomm.poc.core.models.event;

public record EventWrapper<T>(T data, NotificationType type) {
}
