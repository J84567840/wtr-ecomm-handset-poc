package com.waitrose.ecomm.poc.core.services;

import com.waitrose.ecomm.poc.core.models.user.UserCredentials;
import com.waitrose.ecomm.poc.core.models.user.UserSession;

public interface AuthenticationService {

    UserSession authenticate(UserCredentials userCredentials);
}
