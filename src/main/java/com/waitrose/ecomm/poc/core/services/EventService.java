package com.waitrose.ecomm.poc.core.services;

import com.waitrose.ecomm.poc.core.models.event.EventSubscription;
import com.waitrose.ecomm.poc.core.models.event.NotificationType;
import com.waitrose.ecomm.poc.core.models.event.GeneratedSessionEventWrapper;
import com.waitrose.ecomm.poc.core.models.user.UserSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Slf4j
@Service
public class EventService {

    private final CustomerService customerService;
    private final NotificationService notificationService;

    public EventSubscription onboardEventsSubscriber() {
        log.info("Onboarding a new listener");
        EventSubscription eventSubscription = notificationService.subscribeToEvents();

        UserSession userSession = customerService.createAuthenticationSession();

        GeneratedSessionEventWrapper generatedSessionEventWrapper = new GeneratedSessionEventWrapper(userSession);
        notificationService.send(generatedSessionEventWrapper, NotificationType.SESSION);

        return eventSubscription;
    }
}