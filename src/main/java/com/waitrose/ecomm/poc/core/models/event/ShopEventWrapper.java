package com.waitrose.ecomm.poc.core.models.event;

import com.waitrose.ecomm.poc.core.models.shop.ShopDetails;

public record ShopEventWrapper(ShopDetails createShop) {
}
