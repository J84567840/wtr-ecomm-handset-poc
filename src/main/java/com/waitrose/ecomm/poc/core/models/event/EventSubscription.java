package com.waitrose.ecomm.poc.core.models.event;

public interface EventSubscription {

    Object value();
}
