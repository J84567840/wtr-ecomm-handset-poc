package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@EqualsAndHashCode //NOSONAR
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
public class QuantityPrice implements Serializable {

    private String __typename;

    private Quantity quantity;

    private Price price;
}
