package com.waitrose.ecomm.poc.core.models.user;

public record UserCredentials(String username, String password, MyWaitroseCardNo myWaitroseCardNo) {
}
