package com.waitrose.ecomm.poc.core.models.event;

public enum NotificationType {

    SESSION,
    SHOP,
}
