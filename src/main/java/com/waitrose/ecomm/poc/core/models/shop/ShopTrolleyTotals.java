package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Builder
public class ShopTrolleyTotals implements Serializable {

    private String __typename;

    private Price savingsFromMyWaitrose;

    private Price totalEstimatedCost;

    private Integer numberOfItems;
}
