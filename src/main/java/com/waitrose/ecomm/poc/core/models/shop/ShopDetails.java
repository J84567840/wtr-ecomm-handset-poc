package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
@Builder
public class ShopDetails {

    private String __typename;

    private String id;

    private String customerId;

    private String myWtrCardNo;

    private String branchId;

    private String trolleyId;

    //the 4 digit tx number
    private String transactionId;

    private String scoId;

    private String rescanId;

    private String status;

    private String cancelReason;

    private Boolean displayRescanWarning;

    private Date checkedOutAt;

    private Date createdAt;

    private Date updatedAt;

    private ShopTrolleyDetails trolley;
}
