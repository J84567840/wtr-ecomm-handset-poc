package com.waitrose.ecomm.poc.core.models.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@EqualsAndHashCode //NOSONAR
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@ToString
public class Quantity implements Serializable {

    private String __typename;

    private String uom;  // unit of measure

    private float amount;
}
