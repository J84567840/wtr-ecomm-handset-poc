package com.waitrose.ecomm.poc.core.services;

import com.waitrose.ecomm.poc.core.models.shop.BranchId;
import com.waitrose.ecomm.poc.core.models.user.MyWaitroseCardNo;
import com.waitrose.ecomm.poc.core.models.shop.ShopDetails;
import com.waitrose.ecomm.poc.core.models.user.UserSession;

public interface ScanShopService {

    ShopDetails createShop(MyWaitroseCardNo myWaitroseCardNo, BranchId branchId, UserSession userSession);
}
