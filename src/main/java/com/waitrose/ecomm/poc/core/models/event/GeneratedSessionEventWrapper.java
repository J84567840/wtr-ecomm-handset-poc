package com.waitrose.ecomm.poc.core.models.event;

import com.waitrose.ecomm.poc.core.models.user.UserSession;

public record GeneratedSessionEventWrapper(UserSession generateSession) {
}
