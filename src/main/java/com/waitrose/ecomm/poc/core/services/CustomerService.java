package com.waitrose.ecomm.poc.core.services;

import com.waitrose.ecomm.poc.configs.properties.CustomerCredentialsConfig;
import com.waitrose.ecomm.poc.configs.properties.UserCredentialConfig;
import com.waitrose.ecomm.poc.core.models.user.MyWaitroseCardNo;
import com.waitrose.ecomm.poc.core.models.user.UserCredentials;
import com.waitrose.ecomm.poc.core.models.user.UserSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class CustomerService {

    private final CustomerCredentialsConfig customerCredentialsConfig;
    private final AuthenticationService authenticationService;

    public UserSession createAuthenticationSession() {
        Map<String, UserCredentialConfig> userCredentialConfigs = customerCredentialsConfig
                .getCredentials()
                .myWtrCardNoAndCredentials();
        UserCredentialConfig userCredentialConfig = Optional.ofNullable(
                userCredentialConfigs.getOrDefault(customerCredentialsConfig.getStaticTestUser(), null))
                .orElseThrow(() -> new RuntimeException("Misconfigured testUser"));
        MyWaitroseCardNo myWaitroseCardNo = new MyWaitroseCardNo(userCredentialConfig.wtrCardNo());
        return createAuthenticationSession(myWaitroseCardNo);
    }

    public UserSession createAuthenticationSession(MyWaitroseCardNo myWaitroseCardNo) {
        UserCredentialConfig userCredentialConfig = customerCredentialsConfig
                .getCredentials()
                .myWtrCardNoAndCredentials()
                .get(myWaitroseCardNo.value());

        UserCredentials userCredentials = new UserCredentials(
                userCredentialConfig.username(),
                userCredentialConfig.password(),
                myWaitroseCardNo);
        return authenticationService.authenticate(userCredentials);
    }
}
