package com.waitrose.ecomm.poc.configs.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("customer")
public class CustomerCredentialsConfig {

    private String staticTestUser;
    private CredentialConfig credentials;
}
