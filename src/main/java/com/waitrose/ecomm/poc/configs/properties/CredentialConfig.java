package com.waitrose.ecomm.poc.configs.properties;

import java.util.Map;

public record CredentialConfig(Map<String, UserCredentialConfig> myWtrCardNoAndCredentials) {
}
