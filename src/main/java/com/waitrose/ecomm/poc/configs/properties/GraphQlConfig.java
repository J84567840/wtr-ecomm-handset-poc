package com.waitrose.ecomm.poc.configs.properties;

public record GraphQlConfig(String url) {
}
