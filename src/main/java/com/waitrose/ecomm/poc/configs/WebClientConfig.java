package com.waitrose.ecomm.poc.configs;

import com.waitrose.ecomm.poc.configs.properties.ExternalConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Profile(value = "!test")
@Configuration
public class WebClientConfig {

    @Bean
    public WebClient.Builder webClientBuilder(ExternalConfig externalConfig) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
                .fromHttpUrl(externalConfig.getGraphQl().url());
        return WebClient.builder()
                .uriBuilderFactory(new DefaultUriBuilderFactory(uriComponentsBuilder));
    }
}
