package com.waitrose.ecomm.poc.configs.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("!test")
@Data
@Configuration
@ConfigurationProperties("external")
public class ExternalConfig {

    private GraphQlConfig graphQl;
}
