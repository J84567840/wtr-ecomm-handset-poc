package com.waitrose.ecomm.poc.configs.properties;

public record UserCredentialConfig(String username, String password, String wtrCardNo) {
}
