package com.waitrose.ecomm.poc.external.graphql;

import com.waitrose.ecomm.poc.core.services.AuthenticationService;
import com.waitrose.ecomm.poc.core.models.user.UserCredentials;
import com.waitrose.ecomm.poc.external.graphql.models.NewSessionInputRequest;
import com.waitrose.ecomm.poc.core.models.user.UserSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.client.HttpGraphQlClient;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@RequiredArgsConstructor
@Component
public class AuthenticationServiceImpl implements AuthenticationService {

    private final WebClient.Builder webClientBuilder;

    @Override
    public UserSession authenticate(UserCredentials userCredentials) {
        log.info("authenticating user {}", userCredentials.username());

        WebClient webClient = webClientBuilder.build();
        NewSessionInputRequest newSessionInputRequest = new NewSessionInputRequest(
                "WEB_APP",
                userCredentials.password(),
                userCredentials.username());
        HttpGraphQlClient httpGraphQlClient = HttpGraphQlClient.create(webClient);
        return httpGraphQlClient.documentName("sessionPayload")
                .variable("input", newSessionInputRequest)
                .retrieve("generateSession")
                .toEntity(UserSession.class)
                .block();
    }
}
