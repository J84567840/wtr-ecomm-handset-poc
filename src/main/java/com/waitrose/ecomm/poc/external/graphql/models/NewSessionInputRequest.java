package com.waitrose.ecomm.poc.external.graphql.models;

public record NewSessionInputRequest(String clientId, String password, String username) {
}
