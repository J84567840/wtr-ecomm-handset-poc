package com.waitrose.ecomm.poc.external.sse;

import com.waitrose.ecomm.poc.core.services.NotificationService;
import com.waitrose.ecomm.poc.core.models.event.EventSubscription;
import com.waitrose.ecomm.poc.core.models.event.EventWrapper;
import com.waitrose.ecomm.poc.core.models.event.NotificationType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@Component
public class NotificationServiceImpl implements NotificationService {

    private final List<SseEmitter> sseEmitters = new CopyOnWriteArrayList<>();

    @Override
    public EventSubscription subscribeToEvents() {
        SseEmitter sseEmitter = new SseEmitter(Long.MAX_VALUE);
        sseEmitters.add(sseEmitter);
        sseEmitter.onCompletion(() -> {
            log.warn("onCompletion executed {}", sseEmitter);
            sseEmitters.remove(sseEmitter);
        });
        sseEmitter.onTimeout(() -> {
            log.warn("Timeout reached {}", sseEmitter);
            sseEmitters.remove(sseEmitter);
        });
        sseEmitter.onError(error -> {
            //it's duplicated, in theory, while sending the message to the connected sse
            log.warn("Something went wrong {}", error.getMessage());
            sseEmitters.remove(sseEmitter);
        });

        return () -> sseEmitter;
    }

    @Override
    public <T> void send(T notification, NotificationType notificationType) {
        EventWrapper<T> eventWrapper = new EventWrapper<>(notification, notificationType);
        this.sseEmitters.forEach(it -> send(it, eventWrapper));
    }

    private <T> void send(SseEmitter sseEmitter, EventWrapper<T> eventWrapper) {
        try {
            sseEmitter.send(eventWrapper);
        } catch (IOException | RuntimeException e ) {
            sseEmitter.complete();
            sseEmitters.remove(sseEmitter);
            log.error("Something went wrong while sending the notification.", e);
        }
    }
}
