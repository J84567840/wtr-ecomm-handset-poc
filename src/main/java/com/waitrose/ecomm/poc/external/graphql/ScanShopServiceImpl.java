package com.waitrose.ecomm.poc.external.graphql;

import com.waitrose.ecomm.poc.core.services.ScanShopService;
import com.waitrose.ecomm.poc.core.models.shop.BranchId;
import com.waitrose.ecomm.poc.core.models.user.MyWaitroseCardNo;
import com.waitrose.ecomm.poc.core.models.shop.ShopDetails;
import com.waitrose.ecomm.poc.core.models.user.UserSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.client.HttpGraphQlClient;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@RequiredArgsConstructor
@Component
public class ScanShopServiceImpl implements ScanShopService {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String AUTHORIZATION_HEADER_VALUE_PREFIX = "Bearer ";
    private static final String CORRELATION_ID_HEADER = "correlation-id";
    private static final String CORRELATION_ID_HEADER_VALUE = "local-poc-test-1";
    private static final String GRAPH_QL_BRANCH_ID = "branchId";
    private static final String GRAPH_QL_MY_WTR_CARD_NO = "myWtrCardNo";
    private static final String GRAPH_QL_RESPONSE_BODY_NODE = "createShop";
    private static final String GRAPH_QL_COMPLETED_DOCUMENT_SHOP = "completedCreateShopMutation";

    private final WebClient.Builder webClientBuilder;

    @Override
    public ShopDetails createShop(MyWaitroseCardNo myWaitroseCardNo, BranchId branchId, UserSession userSession) {
        WebClient webClient = webClientBuilder
                .defaultHeader(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_VALUE_PREFIX + userSession.accessToken())
                .defaultHeader(CORRELATION_ID_HEADER, CORRELATION_ID_HEADER_VALUE)
                .build();

        log.info("creating a new shop for wtrCardNo {}", myWaitroseCardNo.value());
        
        HttpGraphQlClient httpGraphQlClient = HttpGraphQlClient.create(webClient);
        return httpGraphQlClient.documentName(GRAPH_QL_COMPLETED_DOCUMENT_SHOP)
                .variable(GRAPH_QL_BRANCH_ID, branchId.value())
                .variable(GRAPH_QL_MY_WTR_CARD_NO, myWaitroseCardNo.value())
                .retrieve(GRAPH_QL_RESPONSE_BODY_NODE)
                .toEntity(ShopDetails.class)
                .block();
    }
}
