package com.waitrose.ecomm.poc.endpoint.v1;

import com.waitrose.ecomm.poc.core.services.EventService;
import com.waitrose.ecomm.poc.core.models.event.EventSubscription;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class EventController {

    private final EventService eventService;

    @GetMapping(path = "events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Object subscribeToEvent() {
        EventSubscription eventSubscription = eventService.onboardEventsSubscriber();
        return eventSubscription.value();
    }
}
