package com.waitrose.ecomm.poc.endpoint.v2;

import com.waitrose.ecomm.poc.core.services.ShopService;
import com.waitrose.ecomm.poc.core.models.shop.BranchId;
import com.waitrose.ecomm.poc.core.models.user.MyWaitroseCardNo;
import com.waitrose.ecomm.poc.endpoint.v2.models.request.CreateShopRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class ShopController {

    private final ShopService shopService;

    @PostMapping("/v2/shops")
    public ResponseEntity<Void> createShop(@RequestBody CreateShopRequest createShopRequest) {
        MyWaitroseCardNo myWaitroseCardNo = new MyWaitroseCardNo(createShopRequest.myWtrCardNo());
        BranchId branchId = new BranchId(createShopRequest.branchId());
        shopService.createShop(myWaitroseCardNo, branchId);
        return ResponseEntity.accepted().build();
    }
}
