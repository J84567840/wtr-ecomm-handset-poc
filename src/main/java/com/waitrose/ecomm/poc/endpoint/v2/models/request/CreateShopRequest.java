package com.waitrose.ecomm.poc.endpoint.v2.models.request;

public record CreateShopRequest(String myWtrCardNo, String branchId) {
}
