package com.waitrose.ecomm.poc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waitrose.ecomm.poc.configs.WebClientTest;
import com.waitrose.ecomm.poc.configs.properties.ExternalConfigTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ContextConfiguration;

@AutoConfigureWireMock(stubs = "classpath:/wiremock/mappings", port = 8888)
@ContextConfiguration(classes = { ExternalConfigTest.class, WebClientTest.class })
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractTestRunner {

    @LocalServerPort
    protected int serverPort;
    @Autowired
    protected ObjectMapper objectMapper;
}
