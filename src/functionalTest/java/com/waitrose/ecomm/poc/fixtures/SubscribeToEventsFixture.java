package com.waitrose.ecomm.poc.fixtures;

import com.waitrose.ecomm.poc.AbstractTestRunner;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class SubscribeToEventsFixture extends AbstractTestRunner {

    @Timeout(value = 5)
    @Test
    void shouldSubscribeToTheEventsSuccessfully() throws URISyntaxException, IOException, InterruptedException {
        // given
        var eventsEndpoint = new URI("http://localhost:%s/events".formatted(serverPort));

        var expectedBody = retrieveSubscribeToEventsSuccessful();

        try (var httpClient = HttpClient.newHttpClient()) {
            var getSubscribeToEventsRequest = HttpRequest.newBuilder(eventsEndpoint)
                    .GET()
                    .build();

            // when
            var subscribedBodyResponse = httpClient.send(getSubscribeToEventsRequest, HttpResponse.BodyHandlers.ofLines()).body();
            var firstSseEvent = subscribedBodyResponse.findFirst().orElse(StringUtils.EMPTY);

            // then
            assertThat(firstSseEvent).isEqualToIgnoringWhitespace(expectedBody.stripIndent());

            // teardown
            subscribedBodyResponse.close();
            httpClient.shutdownNow();
        }
    }

    private String retrieveSubscribeToEventsSuccessful() throws IOException {
        try (var expectedSubscribeToEventsSuccessful = getClass()
                .getClassLoader()
                .getResourceAsStream("assertions/subscribe-to-the-events-successful.txt")) {

            return new String(
                    Objects.requireNonNull(expectedSubscribeToEventsSuccessful).readAllBytes(),
                    StandardCharsets.UTF_8);
        }
    }
}
