package com.waitrose.ecomm.poc.fixtures;

import com.waitrose.ecomm.poc.AbstractTestRunner;
import com.waitrose.ecomm.poc.endpoint.v2.models.request.CreateShopRequest;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateShopFixture extends AbstractTestRunner {

    @Timeout(value = 5)
    @Test
    public void shouldCreateShopSuccessfully() throws URISyntaxException, IOException, InterruptedException {
        // given
        var expectedCreatedShop = retrieveExpectedCreatedShop();

        try (var httpClient = HttpClient.newHttpClient()) {
            // and subscribes to events
            var eventsEndpoint = new URI("http://localhost:%s/events".formatted(serverPort));
            var getEventsRequest = HttpRequest.newBuilder(eventsEndpoint)
                    .GET()
                    .build();

            var sseEventsBody = httpClient.send(getEventsRequest, HttpResponse.BodyHandlers.ofLines()).body();

            var createShopUrl = new URI("http://localhost:%s/v2/shops".formatted(serverPort));
            var createShopRequest = new CreateShopRequest("9210315528812364", "257");

            var createShopRequestAsJson = objectMapper.writeValueAsString(createShopRequest);
            var postCreateShopRequest = HttpRequest.newBuilder(createShopUrl)
                    .POST(HttpRequest.BodyPublishers.ofString(createShopRequestAsJson))
                    .header("content-Type", "application/json")
                    .build();

            // when
            var createShopResponse = httpClient.send(postCreateShopRequest, HttpResponse.BodyHandlers.discarding());

            // then
            assertThat(createShopResponse.statusCode()).isEqualTo(HttpStatus.ACCEPTED.value());

            // and checks the events
            var lastSseEvent = sseEventsBody
                    .filter(it -> !it.isEmpty())
                    .limit(2)
                    .reduce((authentication, createdShop) -> createdShop)
                    .orElse(StringUtils.EMPTY);

            assertThat(lastSseEvent).isEqualToIgnoringWhitespace(expectedCreatedShop);

            // teardown
            sseEventsBody.close();
            httpClient.shutdownNow();
        }
    }

    // TODO move to Utils
    private String retrieveExpectedCreatedShop() throws IOException {
        try (var expectedCreatedShopAsFile = getClass()
                .getClassLoader()
                .getResourceAsStream("assertions/create-shop-successfully.txt")) {

            return new String(
                    Objects.requireNonNull(expectedCreatedShopAsFile).readAllBytes(),
                    StandardCharsets.UTF_8);
        }
    }
}
