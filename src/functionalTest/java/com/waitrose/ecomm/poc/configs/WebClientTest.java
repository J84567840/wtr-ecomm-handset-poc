package com.waitrose.ecomm.poc.configs;

import com.waitrose.ecomm.poc.configs.properties.ExternalConfig;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.Request;
import org.eclipse.jetty.http.HttpField;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.reactive.JettyClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.nio.charset.StandardCharsets;

/**
 * It aims to log every response or request
 */
@TestConfiguration
public class WebClientTest {

    @Bean
    public WebClient.Builder webClientBuilder(ExternalConfig externalConfig) {
        HttpClient httpClient = new HttpClient() {
            @Override
            public Request newRequest(URI uri) {
                Request request = super.newRequest(uri);
                return enhance(request);
            }
        };

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
                .fromHttpUrl(externalConfig.getGraphQl().url());
        return WebClient
                .builder()
                .uriBuilderFactory(new DefaultUriBuilderFactory(uriComponentsBuilder))
                .clientConnector(new JettyClientHttpConnector(httpClient));
    }

    Request enhance(Request request) {
        StringBuilder group = new StringBuilder();
        request.onRequestBegin(theRequest -> {
            // append request url and method to group
            group.append("url: %s | method: %s \n".formatted(theRequest.getURI().toString(), theRequest.getMethod()));
        });
        request.onRequestHeaders(theRequest -> {
            for (HttpField header : theRequest.getHeaders()) {
                group.append("header: %s | value: %s \n".formatted(header.getName(), header.getValue()));
            }
        });
        request.onRequestContent((theRequest, content) -> {
            // append content to group
            byte[] bytes = new byte[content.remaining()];
            content.get(bytes);
            String newContent = new String(bytes, StandardCharsets.UTF_8);
            group.append("contentType: %s | body: %s \n".formatted(theRequest.getBody().getContentType(), newContent));
        });
        request.onRequestSuccess(theRequest -> {
            System.out.println(group);
            group.delete(0, group.length());
        });
        group.append("->>> response\n");
        request.onResponseBegin(theResponse -> {
            // append response status to group
        });
        request.onResponseHeaders(theResponse -> {
            for (HttpField header : theResponse.getHeaders()) {
                group.append("header: %s | value: %s \n".formatted(header.getName(), header.getValue()));
            }
        });
        request.onResponseContent((theResponse, content) -> {
            byte[] bytes = new byte[content.remaining()];
            content.get(bytes);
            String newContent = new String(bytes, StandardCharsets.UTF_8);
            group.append("body: %s \n".formatted(newContent));
        });
        request.onResponseSuccess(theResponse -> {
            System.out.println(group);
        });
        return request;
    }
}
