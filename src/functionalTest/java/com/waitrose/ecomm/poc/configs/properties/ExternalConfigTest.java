package com.waitrose.ecomm.poc.configs.properties;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class ExternalConfigTest {

    @Bean
    public ExternalConfig externalConfig() {
        var externalConfig = new ExternalConfig();
        var url = "http://localhost:8888/graphql/graph/live";
        var graphQl = new GraphQlConfig(url);
        externalConfig.setGraphQl(graphQl);
        return externalConfig;
    }
}
